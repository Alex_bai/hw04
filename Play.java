import java.util.ArrayList;

public class Play {

	public static String fileName = "test00.txt";
	public static int capacity = 2;
	public static void main(String args[])
	{
		MarkovDecisionProcess mdp = new MarkovDecisionProcess(fileName, capacity);
		//get carried locations
		mdp.getCarriedLocation();
		//generate all the states
		mdp.generateStates();
		//find start state from states
		Location start = mdp.start;
		mdp.startState = mdp.findState(start.row, start.col, new ArrayList<Location>());
				
	}
}
