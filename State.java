import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class State {

	public int row;
	public int col;
	public int action;
	public List<Location> goals;
	public double utility;
	public double reward;
	public State next;
	
	public State(int row, int col, double utility)
	{
		this.row = row;
		this.col = col;
		this.goals = new ArrayList<Location>();		
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public List<Location> getGoals() {
		return goals;
	}

	public void setGoals(List<Location> goals) {
		this.goals = goals;
	}

	public double getUtility() {
		return utility;
	}

	public void setUtility(double utility) {
		this.utility = utility;
	}

	public double getReward() {
		return reward;
	}

	public void setReward(double reward) {
		this.reward = reward;
	}

	public State getNext() {
		return next;
	}

	public void setNext(State next) {
		this.next = next;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + action;
		result = prime * result + col;
		result = prime * result + ((goals == null) ? 0 : goals.hashCode());
		long temp;
		temp = Double.doubleToLongBits(reward);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + row;
		temp = Double.doubleToLongBits(utility);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (action != other.action)
			return false;
		if (col != other.col)
			return false;
		if (goals == null) {
			if (other.goals != null)
				return false;
		} else{
			if(goals.size() != other.goals.size())
			{
				return false;
			}
			Collections.sort(goals);
			Collections.sort(other.goals);
			for(int i=0;i<goals.size();i++)
			{
				Location loc = goals.get(i);
				Location other_loc = other.goals.get(i);
				if(!loc.equals(other_loc))
				{
					return false;
				}
			}		
		}
		if (Double.doubleToLongBits(reward) != Double.doubleToLongBits(other.reward))
			return false;
		if (row != other.row)
			return false;
		if (Double.doubleToLongBits(utility) != Double.doubleToLongBits(other.utility))
			return false;
		return true;
	}	
	
	public String toString()
	{
		return row+" "+col;
	}
}
