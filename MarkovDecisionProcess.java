import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class MarkovDecisionProcess {

	public int row;
	public int col;
	public int locations[][]; 
	public Location start;
	public State startState;
	public List<Location> goals;
	public State tempState;
	public int capacity;
	public double gamma = 0.9;
	public List<State> states; 
	public final int ACTION_UP = 0;
	public final int ACTION_LEFT = 1;
	public final int ACTION_DOWN = 2;
	public final int ACTION_RIGHT = 3;
	public final double theta = 0.05; 
	
	public MarkovDecisionProcess(String fileName, int capacity)
	{
		this.goals = new ArrayList<Location>();
		this.states = new ArrayList<State>();
		this.capacity = capacity;		
	
		readFile(fileName);		
	}
	
	/**
	 * read the data from file
	 * @param fileName
	 */
	public void readFile(String fileName)
	{		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
			String result= "";
			String data = "";
			while((data=reader.readLine())!=null)
			{
				result += data;
			}
			System.out.println(result);
			
			Scanner scanner = new Scanner(result);
			row = scanner.nextInt();
			col = scanner.nextInt();
			
			locations = new int[row][col];
			for(int i=0;i<row;i++)
			{				
				for(int j=0;j<col;j++)
				{
					int value = scanner.nextInt();
					
					if(value==1)
					{
						//initial the start location
						start = new Location(i, j, value);
					}else if(value >1)
					{
						//get the goal locations
						goals.add(new Location(i, j, value));
					}
					locations[i][j] = value;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * get the carried locations
	 */
	public void getCarriedLocation()
	{
		//sort the goals list on the order of desc
		Collections.sort(goals);
		//get ride of the locations that out of the capacity
		if(capacity < goals.size())
		{
			for(int i=capacity;i<goals.size();i++)
			{
				goals.remove(i);
			}
		}
	}
	
	/**
	 * generate all the states in the maze
	 */
	public void generateStates()
	{
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				if(locations[i][j] != -1)
				{
					State state = new State(i, j, 0);
					states.add(state);
					for(int m=1;m<=capacity;m++)
					{
						tempState = new State(i, j, 0);
						getAllKinds(i, j, m);
					}
				}
			}
		}
	}	
	
	/**
	 * generate all kinds of states 
	 * @param row
	 * @param col
	 * @param m
	 */
	public void getAllKinds(int row, int col, int m)
	{		
		if(m==0)
		{
			if(!states.contains(tempState))
			{
				states.add(tempState);
			}
			tempState = new State(row, col, 0);
			return;
		}
		
		for(int i=0;i<goals.size();i++)
		{
			Location loc = goals.get(i);
			if(!tempState.goals.contains(loc))
			{
				tempState.getGoals().add(loc);
				getAllKinds(row, col, m-1);
			}	
		}
	}
	
	/**
	 * find the start state from states
	 */
	public State findState(int r, int c, List<Location> carried)
	{		
		for(State state : states)
		{
			if(state.row == r && state.col == c && state.goals.equals(carried))
			{
				return state;				
			}
		}
		return null;
	}
	
	/**
	 * generate random policy
	 */
	public void generateRandomPolicy()
	{
		for(State state : states)
		{
			state.action = (int) (Math.random()*4);
		}
		for(State state : states)
		{
			state.next = getNextState(state);
		}
	}
	
	public State getNextState(State state)
	{
		int action = state.action;
		int r = state.row;
		int c = state.col;
		switch(state.action)
		{
			case 0:
				r--;
				break;
			case 1:
				c--;
				break;
			case 2:
				r++;
				break;
			case 3:
				c++;
				break;
		}
		//if it is out of boundary
		if(r<0 || r>=row || c<0 || c>=col)
		{
			return null;
		}
		
		//get next location value
		int locValue = locations[r][c];
		//if next location is obstacle
		if(locValue == -1)
		{
			return null;
		}
		List<Location> carried = state.goals;
		for(Location loc : goals)
		{
			//if next location is goal
			if(loc.row==r && loc.col==c)
			{
				carried.add(loc);
			}
		}
		return findState(r, c, carried);
	}
	
}









